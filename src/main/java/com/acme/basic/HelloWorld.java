package com.acme.basic;

import org.checkerframework.checker.guieffect.qual.SafeEffect;
import org.checkerframework.checker.units.qual.K;

public class HelloWorld {
  private KanWorker kanWorker;
  public HelloWorld() {
    this.kanWorker = new KanWorker();
  }

  void sayHello() {
    System.out.println("Hello World!");
  }

  void notCovered() {
    System.out.println("This method is not covered by unit tests");
  }

  String helloAgain() {
    return "Hello";
  }

}

package com.acme.basic;

public class SuperAIChatBot {
    private KanWorker kanWorker;
    public SuperAIChatBot() {
        this.kanWorker = new KanWorker();
    }

    String speakToChatBot(String inMsg) {
        if (inMsg == null) {
            return "Say something";
        } else {
            return this.aiReply(inMsg);
        }
    }

    String aiReply(String inMsg) {
        if (inMsg == null) {
            return "Pardon";
        }
        if (inMsg.contains("fxck")) {
            return "Be polite";
        } else if (inMsg.contains("love")) {
            return "Sorry! I'm not available";
        } else if (inMsg.contains("orderNumber")) {
            // Just for demo <mockito verify times = 2> for determineOrderForThreeInteger
            String dummyString = kanWorker.determineOrderForThreeInteger(1,2,3);
            return kanWorker.determineOrderForThreeInteger(1,2,3);
        } else if (inMsg.contains("Convert")) {
            return kanWorker.convertNumberToMonth(1); // Just for demo: hardcode 1
        } else {
            // Just duplicate for code quality demo
            if (inMsg.contains("fuck")) {
                return "Be polite";
            } else if (inMsg.contains("love")) {
                return "Sorry! I'm not available";
            } else {
                return "You're quite boring";
            }
        }
    }



    // Just duplicate for code quality demo
    String replyDup(String inMsg) {
        if (inMsg == null) {
            return "Pardon";
        }
        if (inMsg.contains("fxck")) {
            return "Be polite";
        } else if (inMsg.contains("love")) {
            return "Sorry! I'm not available";
        } else if (inMsg.contains("orderNumber")) {
            // Just for demo <mockito verify times = 2> for determineOrderForThreeInteger
            String dummyString = kanWorker.determineOrderForThreeInteger(1,2,3);
            return kanWorker.determineOrderForThreeInteger(1,2,3);
        } else if (inMsg.contains("Convert")) {
            return kanWorker.convertNumberToMonth(1); // Just for demo: hardcode 1
        } else {
            // Just duplicate for code quality demo
            if (inMsg.contains("fuck")) {
                return "Be polite";
            } else if (inMsg.contains("love")) {
                return "Sorry! I'm not available";
            } else {
                return "You're quite boring";
            }
        }
    }
}

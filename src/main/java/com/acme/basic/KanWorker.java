package com.acme.basic;

import java.security.SecureRandom;

public class KanWorker {
  String determineOrderForThreeInteger(final int a, final int b, final int c) {
    String result=null;

    if (a > b) {
      if (b > c) {
        result = "a > b > c";
      } else {
        if (c > a) {
          result = "c > a > b";
        } else {
          result = "a > c > b";
        }
      }
    } else { // b > a
      if (a > c) {
        result = "b > a > c";
      } else {
        if (c > b) {
          result = "c > b > a";
        } else {
          result = "b > c > a";
        }
      }
    }


    // Intended duplication for code quality check detection
    if (a > b) {
      if (b > c) {
        result = "a > b > c";
      } else {
        if (c > a) {
          result = "c > a > b";
        } else {
          result = "a > c > b";
        }
      }
    } else { // b > a
      if (a > c) {
        result = "b > a > c";
      } else {
        if (c > b) {
          result = "c > b > a";
        } else {
          result = "b > c > a";
        }
      }
    }

    return result;
  }

   String convertNumberToMonth(final int monthNumber) {
    switch (monthNumber) {
      case 1:
        return "Jan";
      case 2:
        return "Feb";
      case 3:
        return "Feb"; // intended bug for testing demo
      case 4:
        return "Apr";
      case 5:
        return "May";
      case 6:
        return "Jun";
      case 7:
        return "Jul";
      case 8:
        return "Aug";
      case 9:
        return "Sep";
      case 10:
        return "Oct";
      case 11:
        return "Nov";
      case 12:
        return "Dec";
      default:
        return "Unknown";
    }
  }

  void poorCode() {
    // never running loop
    for(int i=20;i<=10;i++){
      System.out.println(i);
    }

    byte[] randomBytes = new byte[8];
    SecureRandom random = new SecureRandom();
    random.nextBytes(randomBytes);
  }

}

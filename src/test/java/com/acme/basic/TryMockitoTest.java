package com.acme.basic;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import java.util.List;

import static org.junit.Assert.assertNotEquals;
import static org.mockito.Mockito.*;
import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyInt;

public class TryMockitoTest {
    @Before
    public void initialize() {
        MockitoAnnotations.initMocks(this);
    }
    @Mock
    KanWorker mockKanWorker;
    @InjectMocks
    SuperAIChatBot codeUnderTestSuperAIChatBot;
    @Test
    public void testDetermineOrderCalled() {
        when(mockKanWorker.determineOrderForThreeInteger(anyInt(), anyInt(), anyInt())).thenReturn("calledDeterOrder");
        assertEquals("calledDeterOrder", codeUnderTestSuperAIChatBot.aiReply("orderNumber"));
        verify(mockKanWorker, times(2)).determineOrderForThreeInteger(anyInt(), anyInt(), anyInt());
    }
    @Test
    public void testConvertNumberToMonth() {
        when(mockKanWorker.convertNumberToMonth(anyInt())).thenReturn("calledToMonth");
        assertEquals("calledToMonth", codeUnderTestSuperAIChatBot.aiReply("Convert: 1"));
    }
    @Test
    public void testConvertNumberToMonthWithoutMock() {
        assertEquals("Jan", new SuperAIChatBot().aiReply("Convert: 1"));
    }


    @Test
    public void mockitoDemoTest() {
        List<String> mockList = mock(List.class);
        when(mockList.get(0)).thenReturn("Mockito");
        when(mockList.get(1)).thenReturn("JCG");
        mockList.add("First");
        assertEquals("Mockito", mockList.get(0));
        assertEquals("JCG", mockList.get(1));
    }

}

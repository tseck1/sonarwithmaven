package com.acme.basic;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import java.util.Collection;
import java.util.Arrays;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class DetermineNumberTest {
    private int a, b, c;
    String expectedResult;
    private KanWorker kanworker;

    public DetermineNumberTest(int a, int b, int c, String expectedResult) {
        super();
        this.a = a;
        this.b = b;
        this.c = c;
        this.expectedResult = expectedResult;
    }

    @Before
    public void initialize() {
        kanworker =  new KanWorker();
    }

    @Parameterized.Parameters
    public static Collection input() {
        return Arrays.asList(new Object[][] {
                {3,2,1,"a > b > c"},
                {1,2,3,"c > b > a"},
                {5,2,3,"a > c > b"},
                {5,8,2,"b > a > c"},
                {0,5,100,"c > b > a"},
                {-2,-6,-15,"a > b > c"},
                {-2,0,15,"c > b > a"},
                {5000,-9999,1,"a > c > b"}
        });
    }

    @Test
    public void testNumberOrder() {
        assertEquals(expectedResult,  kanworker.determineOrderForThreeInteger(a, b, c));
    }
}




package com.acme.basic;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class SuperAIChatBotIT {
    private SuperAIChatBot superAIChatBot;

    @Before
    public void initialize() {
        superAIChatBot =  new SuperAIChatBot();
    }

    @Test
    public void sayNothing() {
        assertNotEquals("Say something",  superAIChatBot.speakToChatBot(null));
    }
}

package com.acme.basic;

import org.junit.Test;
import org.junit.Assert;

import static org.junit.Assert.assertEquals;

public class HelloWorldTest {

  @Test
  public void sayHello() {
    new HelloWorld().sayHello();
  }

  @Test
  public void checkHelloAgain2() {
    assertEquals("Hello",  new HelloWorld().helloAgain());
  }
}

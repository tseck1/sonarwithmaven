package com.acme.basic;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class SuperAIChatBotTest {
  private SuperAIChatBot superAIChatBot;

  @Before
  public void initialize() {
    superAIChatBot =  new SuperAIChatBot();
  }

  @Test
  public void sayNothing() {
    assertEquals("Say something",  superAIChatBot.speakToChatBot(null));
  }

  @Test
  public void sayffff() {
    assertEquals("Be polite",  superAIChatBot.speakToChatBot("fxck"));
  }

  @Test
  public void sayFFFF() {assertNotEquals("Be polite",  superAIChatBot.speakToChatBot("Fxck"));}

  @Test
  public void sayff_ff() {
    assertNotEquals("Be polite",  superAIChatBot.speakToChatBot("fx ck"));
  }

  @Test
  public void loveMsg() {
    assertEquals("Sorry! I'm not available",  superAIChatBot.aiReply("I love you"));
  }

}
